#include "FreeRTOS.h"
#include "queue.h"
#include "stm32f1xx_hal.h"
#include "mk_uart_helper.h"

extern QueueHandle_t mk_bluetoothQueue;

void mk_initBluetoothHandler();

HAL_StatusTypeDef mk_receiveBluetoothIT(UART_HandleTypeDef *huart);

void mk_readBluetoothString(char * data);
