#include "FreeRTOS.h"
#include "queue.h"
#include "stm32f1xx_hal.h"

HAL_StatusTypeDef mk_UART_Receive_IT(UART_HandleTypeDef *huart, QueueHandle_t xQueue);

void mk_readQueueString(char * str, QueueHandle_t xQueue);

void mk_sendString(UART_HandleTypeDef *huart1, char * data);
