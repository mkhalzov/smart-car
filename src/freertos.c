/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  *
  * COPYRIGHT(c) 2016 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#include "gpio.h"
#include "usart.h"

#include <string.h>

#include "mk_bluetooth_handler.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId sensorsHandle;
osThreadId bluetoothHandle;
osThreadId motorsHandle;
osMessageQId bluetoothQueueHandle;
osMessageQId sonar1QueueHandle;
osMessageQId sonar2QueueHandle;

/* USER CODE BEGIN Variables */

/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartSensorsHandler(void const * argument);
void StartBluetoothHandler(void const * argument);
void StartMotorsHandler(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of sensorsHandler */
  osThreadDef(sensorsHandler, StartSensorsHandler, osPriorityNormal, 0, 128);
  sensorsHandle = osThreadCreate(osThread(sensorsHandler), NULL);

  /* definition and creation of bluetoothHandle */
  osThreadDef(bluetoothHandle, StartBluetoothHandler, osPriorityIdle, 0, 1024);
  bluetoothHandle = osThreadCreate(osThread(bluetoothHandle), NULL);

  /* definition and creation of motorsHandler */
  osThreadDef(motorsHandler, StartMotorsHandler, osPriorityIdle, 0, 128);
  motorsHandle = osThreadCreate(osThread(motorsHandler), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Create the queue(s) */
  /* definition and creation of bluetoothQueue */
  osMessageQDef(bluetoothQueue, 512, uint8_t);
  bluetoothQueueHandle = osMessageCreate(osMessageQ(bluetoothQueue), NULL);

  /* definition and creation of sonar1Queue */
  osMessageQDef(sonar1Queue, 16, uint16_t);
  sonar1QueueHandle = osMessageCreate(osMessageQ(sonar1Queue), NULL);

  /* definition and creation of sonar2Queue */
  osMessageQDef(sonar2Queue, 16, uint16_t);
  sonar2QueueHandle = osMessageCreate(osMessageQ(sonar2Queue), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
}

/* StartSensorsHandler function */
void StartSensorsHandler(void const * argument)
{

  /* USER CODE BEGIN StartSensorsHandler */
  /* Infinite loop */
  for(;;)
  {
      HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
      osDelay(1000);
      HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);
      osDelay(1000);
  }
  /* USER CODE END StartSensorsHandler */
}

/* StartBluetoothHandler function */
void StartBluetoothHandler(void const * argument)
{
  /* USER CODE BEGIN StartBluetoothHandler */

  mk_initBluetoothHandler();

  /** TODO this is the problem! I don't use strings properly. Fix this! */

  char command[20];
  int commandIndex = 0;
  char c;
  //int i;
  /* Infinite loop */
  for(;;)
  {
/** TODO here is stops */
    //mk_readBluetoothString(cmd);


    // TODO might be used for debug
    //int queueLength = uxQueueMessagesWaiting(mk_bluetoothQueue);
    char data[512];
    int i=0;
    while(uxQueueMessagesWaiting(mk_bluetoothQueue)) {
        char c;
        if( xQueueReceive( mk_bluetoothQueue, &c, ( TickType_t ) 10 ) )
        {
          data[i++%512] = c;
        }
    }
    data[i] = 0;

    int len = strlen(data);

    // get command
    if (len) {
      for (i=0; i<len; i++) {
        c = data[i];

        if (c == '\r') continue;

        if (c == '\n') {
          command[commandIndex] = '\0';

          char response[20];
          sprintf(response,"%s\r\n", command);
          mk_sendString(&huart1, response);

          // TODO handle command here !

          // clear command index
          command[commandIndex = 0] = '\0';
          continue;
        } else {
          command[commandIndex++%512] = c;
        }
      }
    }

    osDelay(100);

  }
  /* USER CODE END StartBluetoothHandler */
}

/* StartMotorsHandler function */
void StartMotorsHandler(void const * argument)
{
  /* USER CODE BEGIN StartMotorsHandler */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END StartMotorsHandler */
}

/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
