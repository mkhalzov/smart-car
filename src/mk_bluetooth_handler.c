#include "mk_bluetooth_handler.h"

QueueHandle_t mk_bluetoothQueue;

void mk_initBluetoothHandler(void) {
//  osMessageQDef(bluetoothQueue, 512, uint8_t);
//  bluetoothQueueHandle = osMessageCreate(osMessageQ(bluetoothQueue), NULL);

  // Create a queue capable of containing 10 unsigned long values.
  mk_bluetoothQueue = xQueueCreate( 512, sizeof( uint8_t ) );
  if( mk_bluetoothQueue == 0 )
  {
      // Queue was not created and must not be used.
      // TODO do something here!
      //printf("Queue was not created and must not be used.\n");
  }

}

HAL_StatusTypeDef mk_receiveBluetoothIT(UART_HandleTypeDef *huart) {
  return mk_UART_Receive_IT(huart, mk_bluetoothQueue);
}

void mk_readBluetoothString(char * data) {
  mk_readQueueString(data, mk_bluetoothQueue);
}
