#include "mk_uart_helper.h"

#include <string.h>



HAL_StatusTypeDef mk_UART_Receive_IT(UART_HandleTypeDef *huart, QueueHandle_t xQueue) {

    // TODO
  uint8_t rxByte;
  // TODO unknown size of data receiving
  // TODO receiving many times some zero value
/*  if (HAL_UART_Receive_IT(huart, &rxByte, sizeof(rxByte))) {
      xQueueSendFromISR(xQueue, &rxByte, NULL);
      return HAL_OK;
  } else {
      return HAL_ERROR;
  };
*/

  uint32_t tmp1, tmp2;

  tmp1 = __HAL_UART_GET_FLAG(huart, UART_FLAG_RXNE);
  tmp2 = __HAL_UART_GET_IT_SOURCE(huart, UART_IT_RXNE);
  // UART in mode Receiver ---------------------------------------------------
  if((tmp1 != RESET) && (tmp2 != RESET))
  {
      if((huart->State == HAL_UART_STATE_BUSY_RX) || (huart->State == HAL_UART_STATE_BUSY_TX_RX))
      {
          if(huart->Init.WordLength == UART_WORDLENGTH_9B)
          {
              return HAL_ERROR; //implement your logic to handle 9B words
          }
          else
          {
              if(huart->Init.Parity == UART_PARITY_NONE)
              {
                  rxByte = (uint8_t)(huart->Instance->DR & (uint8_t)0x00FF);
              }
              else
              {
                  rxByte = (uint8_t)(huart->Instance->DR & (uint8_t)0x007F);
              }
          }
          // TODO rxByte is always empty symbol \0
          xQueueSendFromISR(xQueue, &rxByte, NULL);
      }
  }
  else
  {
      HAL_UART_IRQHandler(huart); // default HAL handler
  }

  if (huart->State == HAL_UART_STATE_BUSY_TX)
  {
      huart->State = HAL_UART_STATE_BUSY_TX_RX;
  }
  else
  {
      huart->State = HAL_UART_STATE_BUSY_RX;
  }

  __HAL_UART_ENABLE_IT(huart, UART_IT_RXNE);

  return HAL_OK;

}

/**
 * TODO return some status ?
 */
void mk_readQueueString(char * str, QueueHandle_t xQueue) {

  // TODO might be used for debug
  //int queueLength = uxQueueMessagesWaiting(xQueue);
  //printf("%d",queueLength);

  int i=0;

  //while(!RingBuffer_IsEmpty(&rBuffer)) {
  while(uxQueueMessagesWaiting(xQueue)) {
    //char c = RingBuffer_GetByte(&rBuffer);
    char c;
    if( xQueueReceive( xQueue, &c, ( TickType_t ) 10 ) )
    {
      str[i++%512] = c;
    }
  }

  /** TODO this is the problem! I don't use strings properly. Fix this! */
  str[i] = 0;
}

void mk_sendString(UART_HandleTypeDef *huart1, char * data) {
  /**  TODO get huart somehow
   * TODO fix the compiler warning message
   */
  int len = strlen(data);
  HAL_UART_Transmit(huart1, data, len, 100);
}
